#pragma once
#include <cstdint>
#include <array>
#include "olc6502.hpp"
/* cstdint renames primitive types into explicit types such as signed, unsigned and their sizes */

class Bus {
    public:
        Bus();
        ~Bus();

        // Devices on bus
        /* A generic 6502 CPU implementation that could be used for multiple projects */
        olc6502 cpu;

        /* RAM allocation */
        std::array<uint8_t, 64 * 1024> ram;

        /* The NES 2A03 (which is really a 6502 + the audio processing unit) is capable of operating on 8 bit data and 16 bit addresses */
        void write(uint16_t addr, uint8_t data);
        uint8_t read(uint16_t addr, bool bReadOnly = false);
};
