#pragma once
#include <cstdint>
#include <string>
#include <vector>

class Bus;

class olc6502 {
public:
    olc6502();
    ~olc6502();

    void ConnectBus(Bus *n) {
        bus = n;
    }

    /* Flags */
    enum FLAGS6502 {
        C = (1 << 0), // Carry bit
        Z = (1 << 1), // Zero
        I = (1 << 2), // Disable interrupts
        D = (1 << 3), // Decimal mode (unused)
        B = (1 << 4), // Break
        U = (1 << 5), // Unused
        /* V and N are only used with signed variables */
        V = (1 << 6), // Overflow
        N = (1 << 7), // Negative
    };

    /* Registers */
    uint8_t a = 0x00; // Accumulator
    uint8_t x = 0x00; // X register
    uint8_t y = 0x00; // Y register
    uint8_t stkp = 0x00; // Stack pointer (points to location on the bus)
    uint16_t pc = 0x00; // Program counter (16 bit)
    uint8_t status = 0x00; // Status register

    /* Addressing modes, 12 */
    uint8_t IMP(); uint8_t IMM();
    uint8_t ZP0(); uint8_t ZPX();
    uint8_t ZPY(); uint8_t REL();
    uint8_t ABS(); uint8_t ABX();
    uint8_t ABY(); uint8_t IND();
    uint8_t IZX(); uint8_t IZY();

    /* Instructions, 56 in total */
    uint8_t ADC();  uint8_t AND();  uint8_t ASL();  uint8_t BCC();
    uint8_t BCS();  uint8_t BEQ();  uint8_t BIT();  uint8_t BMI();
    uint8_t BNE();  uint8_t BPL();  uint8_t BRK();  uint8_t BVC();
    uint8_t BVS();  uint8_t CLC();  uint8_t CLD();  uint8_t CLI();
    uint8_t CLV();  uint8_t CMP();  uint8_t CPX();  uint8_t CPY();
    uint8_t DEC();  uint8_t DEX();  uint8_t DEY();  uint8_t EOR();
    uint8_t INC();  uint8_t INX();  uint8_t INY();  uint8_t JMP();
    uint8_t JSR();  uint8_t LDA();  uint8_t LDX();  uint8_t LDY();
    uint8_t LSR();  uint8_t NOP();  uint8_t ORA();  uint8_t PHA();
    uint8_t PHP();  uint8_t PLA();  uint8_t PLP();  uint8_t ROL();
    uint8_t ROR();  uint8_t RTI();  uint8_t RTS();  uint8_t SBC();
    uint8_t SEC();  uint8_t SED();  uint8_t SEI();  uint8_t STA();
    uint8_t STX();  uint8_t STY();  uint8_t TAX();  uint8_t TAY();
    uint8_t TSX();  uint8_t TXA();  uint8_t TXS();  uint8_t TYA();

    /* All illegal / unofficial instructions are caught by this function */
    uint8_t XXX();

    /* External signals */

    /* Indicate number of clock cycles to occur to the CPU */
    void clock();

    /* Interrupts */

    /* Resets CPU state */
    void reset();
    /* Interrupt request
     * Standard interrupts can be ignored based on if the
     * interrupt enable flag has been set or not */
    void irq();
    /* Non-maskable interrupt - can never be disabled */
    void nmi();

    /* Internal functionality */

    /* It's possible that the instructions use data and we need to fetch
     * that data from appropriate source */
    uint8_t fetch();
    /* Store fetched data */
    uint8_t fetched = 0x00;

    /* Based on the addressing mode, we may want to read from different
     * locations of memory. We use this variable to store that location. */
    uint16_t addr_abs = 0x0000;
    /* On the 6502, branch instructions can only jump a certain distance
     * from where the branch instruction was called. So they jump to a
     * relative address. */
    uint16_t addr_rel = 0x00;
    /* Store the opcode of the currently executed instruction / mnemonic */
    uint8_t instruction = 0x00;
    /* Number of cycles left for the duration of the currently executed
     * instruction */
    uint8_t cycles = 0;

private:
    Bus *bus = nullptr;
    uint8_t read(uint16_t a);
    void write(uint16_t a, uint8_t d);

    /* Convenience functions to access status register */
    uint8_t GetFlag(FLAGS6502 f);
    void SetFlag(FLAGS6502 f, bool v);

    /* Metadata for a instruction - for disassembly*/
    struct INSTRUCTION {
        /* Name of the mnemonic */
        std::string name;
        /* Function pointer for operation to be performed */
        uint8_t(olc6502::*operate)(void) = nullptr;
        /* Function pointer for address mode of the operation */
        uint8_t(olc6502::*addrmode)(void) = nullptr;
        /* The number of cycles required for said operation */
        uint8_t cycles = 0;
    };

    /* 16 x 16 Instruction lookup table containing items in the following order:
     * - Mnemonic
     * - Function pointer to the function implementing the instruction
     * - Function pointer to the function implementing the addressing mode
     * - Number of cycles required */
    std::vector<INSTRUCTION> lookup;
};
