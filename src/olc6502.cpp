#include "olc6502.hpp"

olc6502::olc6502() {
    using a = olc6502;
    lookup = {
    { "BRK", &a::BRK, &a::IMM, 7 },{ "ORA", &a::ORA, &a::IZX, 6 },{ "???", &a::XXX, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 8 },{ "???", &a::NOP, &a::IMP, 3 },{ "ORA", &a::ORA, &a::ZP0, 3 },{ "ASL", &a::ASL, &a::ZP0, 5 },{ "???", &a::XXX, &a::IMP, 5 },{ "PHP", &a::PHP, &a::IMP, 3 },{ "ORA", &a::ORA, &a::IMM, 2 },{ "ASL", &a::ASL, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 2 },{ "???", &a::NOP, &a::IMP, 4 },{ "ORA", &a::ORA, &a::ABS, 4 },{ "ASL", &a::ASL, &a::ABS, 6 },{ "???", &a::XXX, &a::IMP, 6 },
    { "BPL", &a::BPL, &a::REL, 2 },{ "ORA", &a::ORA, &a::IZY, 5 },{ "???", &a::XXX, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 8 },{ "???", &a::NOP, &a::IMP, 4 },{ "ORA", &a::ORA, &a::ZPX, 4 },{ "ASL", &a::ASL, &a::ZPX, 6 },{ "???", &a::XXX, &a::IMP, 6 },{ "CLC", &a::CLC, &a::IMP, 2 },{ "ORA", &a::ORA, &a::ABY, 4 },{ "???", &a::NOP, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 7 },{ "???", &a::NOP, &a::IMP, 4 },{ "ORA", &a::ORA, &a::ABX, 4 },{ "ASL", &a::ASL, &a::ABX, 7 },{ "???", &a::XXX, &a::IMP, 7 },
    { "JSR", &a::JSR, &a::ABS, 6 },{ "AND", &a::AND, &a::IZX, 6 },{ "???", &a::XXX, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 8 },{ "BIT", &a::BIT, &a::ZP0, 3 },{ "AND", &a::AND, &a::ZP0, 3 },{ "ROL", &a::ROL, &a::ZP0, 5 },{ "???", &a::XXX, &a::IMP, 5 },{ "PLP", &a::PLP, &a::IMP, 4 },{ "AND", &a::AND, &a::IMM, 2 },{ "ROL", &a::ROL, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 2 },{ "BIT", &a::BIT, &a::ABS, 4 },{ "AND", &a::AND, &a::ABS, 4 },{ "ROL", &a::ROL, &a::ABS, 6 },{ "???", &a::XXX, &a::IMP, 6 },
    { "BMI", &a::BMI, &a::REL, 2 },{ "AND", &a::AND, &a::IZY, 5 },{ "???", &a::XXX, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 8 },{ "???", &a::NOP, &a::IMP, 4 },{ "AND", &a::AND, &a::ZPX, 4 },{ "ROL", &a::ROL, &a::ZPX, 6 },{ "???", &a::XXX, &a::IMP, 6 },{ "SEC", &a::SEC, &a::IMP, 2 },{ "AND", &a::AND, &a::ABY, 4 },{ "???", &a::NOP, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 7 },{ "???", &a::NOP, &a::IMP, 4 },{ "AND", &a::AND, &a::ABX, 4 },{ "ROL", &a::ROL, &a::ABX, 7 },{ "???", &a::XXX, &a::IMP, 7 },
    { "RTI", &a::RTI, &a::IMP, 6 },{ "EOR", &a::EOR, &a::IZX, 6 },{ "???", &a::XXX, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 8 },{ "???", &a::NOP, &a::IMP, 3 },{ "EOR", &a::EOR, &a::ZP0, 3 },{ "LSR", &a::LSR, &a::ZP0, 5 },{ "???", &a::XXX, &a::IMP, 5 },{ "PHA", &a::PHA, &a::IMP, 3 },{ "EOR", &a::EOR, &a::IMM, 2 },{ "LSR", &a::LSR, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 2 },{ "JMP", &a::JMP, &a::ABS, 3 },{ "EOR", &a::EOR, &a::ABS, 4 },{ "LSR", &a::LSR, &a::ABS, 6 },{ "???", &a::XXX, &a::IMP, 6 },
    { "BVC", &a::BVC, &a::REL, 2 },{ "EOR", &a::EOR, &a::IZY, 5 },{ "???", &a::XXX, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 8 },{ "???", &a::NOP, &a::IMP, 4 },{ "EOR", &a::EOR, &a::ZPX, 4 },{ "LSR", &a::LSR, &a::ZPX, 6 },{ "???", &a::XXX, &a::IMP, 6 },{ "CLI", &a::CLI, &a::IMP, 2 },{ "EOR", &a::EOR, &a::ABY, 4 },{ "???", &a::NOP, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 7 },{ "???", &a::NOP, &a::IMP, 4 },{ "EOR", &a::EOR, &a::ABX, 4 },{ "LSR", &a::LSR, &a::ABX, 7 },{ "???", &a::XXX, &a::IMP, 7 },
    { "RTS", &a::RTS, &a::IMP, 6 },{ "ADC", &a::ADC, &a::IZX, 6 },{ "???", &a::XXX, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 8 },{ "???", &a::NOP, &a::IMP, 3 },{ "ADC", &a::ADC, &a::ZP0, 3 },{ "ROR", &a::ROR, &a::ZP0, 5 },{ "???", &a::XXX, &a::IMP, 5 },{ "PLA", &a::PLA, &a::IMP, 4 },{ "ADC", &a::ADC, &a::IMM, 2 },{ "ROR", &a::ROR, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 2 },{ "JMP", &a::JMP, &a::IND, 5 },{ "ADC", &a::ADC, &a::ABS, 4 },{ "ROR", &a::ROR, &a::ABS, 6 },{ "???", &a::XXX, &a::IMP, 6 },
    { "BVS", &a::BVS, &a::REL, 2 },{ "ADC", &a::ADC, &a::IZY, 5 },{ "???", &a::XXX, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 8 },{ "???", &a::NOP, &a::IMP, 4 },{ "ADC", &a::ADC, &a::ZPX, 4 },{ "ROR", &a::ROR, &a::ZPX, 6 },{ "???", &a::XXX, &a::IMP, 6 },{ "SEI", &a::SEI, &a::IMP, 2 },{ "ADC", &a::ADC, &a::ABY, 4 },{ "???", &a::NOP, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 7 },{ "???", &a::NOP, &a::IMP, 4 },{ "ADC", &a::ADC, &a::ABX, 4 },{ "ROR", &a::ROR, &a::ABX, 7 },{ "???", &a::XXX, &a::IMP, 7 },
    { "???", &a::NOP, &a::IMP, 2 },{ "STA", &a::STA, &a::IZX, 6 },{ "???", &a::NOP, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 6 },{ "STY", &a::STY, &a::ZP0, 3 },{ "STA", &a::STA, &a::ZP0, 3 },{ "STX", &a::STX, &a::ZP0, 3 },{ "???", &a::XXX, &a::IMP, 3 },{ "DEY", &a::DEY, &a::IMP, 2 },{ "???", &a::NOP, &a::IMP, 2 },{ "TXA", &a::TXA, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 2 },{ "STY", &a::STY, &a::ABS, 4 },{ "STA", &a::STA, &a::ABS, 4 },{ "STX", &a::STX, &a::ABS, 4 },{ "???", &a::XXX, &a::IMP, 4 },
    { "BCC", &a::BCC, &a::REL, 2 },{ "STA", &a::STA, &a::IZY, 6 },{ "???", &a::XXX, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 6 },{ "STY", &a::STY, &a::ZPX, 4 },{ "STA", &a::STA, &a::ZPX, 4 },{ "STX", &a::STX, &a::ZPY, 4 },{ "???", &a::XXX, &a::IMP, 4 },{ "TYA", &a::TYA, &a::IMP, 2 },{ "STA", &a::STA, &a::ABY, 5 },{ "TXS", &a::TXS, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 5 },{ "???", &a::NOP, &a::IMP, 5 },{ "STA", &a::STA, &a::ABX, 5 },{ "???", &a::XXX, &a::IMP, 5 },{ "???", &a::XXX, &a::IMP, 5 },
    { "LDY", &a::LDY, &a::IMM, 2 },{ "LDA", &a::LDA, &a::IZX, 6 },{ "LDX", &a::LDX, &a::IMM, 2 },{ "???", &a::XXX, &a::IMP, 6 },{ "LDY", &a::LDY, &a::ZP0, 3 },{ "LDA", &a::LDA, &a::ZP0, 3 },{ "LDX", &a::LDX, &a::ZP0, 3 },{ "???", &a::XXX, &a::IMP, 3 },{ "TAY", &a::TAY, &a::IMP, 2 },{ "LDA", &a::LDA, &a::IMM, 2 },{ "TAX", &a::TAX, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 2 },{ "LDY", &a::LDY, &a::ABS, 4 },{ "LDA", &a::LDA, &a::ABS, 4 },{ "LDX", &a::LDX, &a::ABS, 4 },{ "???", &a::XXX, &a::IMP, 4 },
    { "BCS", &a::BCS, &a::REL, 2 },{ "LDA", &a::LDA, &a::IZY, 5 },{ "???", &a::XXX, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 5 },{ "LDY", &a::LDY, &a::ZPX, 4 },{ "LDA", &a::LDA, &a::ZPX, 4 },{ "LDX", &a::LDX, &a::ZPY, 4 },{ "???", &a::XXX, &a::IMP, 4 },{ "CLV", &a::CLV, &a::IMP, 2 },{ "LDA", &a::LDA, &a::ABY, 4 },{ "TSX", &a::TSX, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 4 },{ "LDY", &a::LDY, &a::ABX, 4 },{ "LDA", &a::LDA, &a::ABX, 4 },{ "LDX", &a::LDX, &a::ABY, 4 },{ "???", &a::XXX, &a::IMP, 4 },
    { "CPY", &a::CPY, &a::IMM, 2 },{ "CMP", &a::CMP, &a::IZX, 6 },{ "???", &a::NOP, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 8 },{ "CPY", &a::CPY, &a::ZP0, 3 },{ "CMP", &a::CMP, &a::ZP0, 3 },{ "DEC", &a::DEC, &a::ZP0, 5 },{ "???", &a::XXX, &a::IMP, 5 },{ "INY", &a::INY, &a::IMP, 2 },{ "CMP", &a::CMP, &a::IMM, 2 },{ "DEX", &a::DEX, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 2 },{ "CPY", &a::CPY, &a::ABS, 4 },{ "CMP", &a::CMP, &a::ABS, 4 },{ "DEC", &a::DEC, &a::ABS, 6 },{ "???", &a::XXX, &a::IMP, 6 },
    { "BNE", &a::BNE, &a::REL, 2 },{ "CMP", &a::CMP, &a::IZY, 5 },{ "???", &a::XXX, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 8 },{ "???", &a::NOP, &a::IMP, 4 },{ "CMP", &a::CMP, &a::ZPX, 4 },{ "DEC", &a::DEC, &a::ZPX, 6 },{ "???", &a::XXX, &a::IMP, 6 },{ "CLD", &a::CLD, &a::IMP, 2 },{ "CMP", &a::CMP, &a::ABY, 4 },{ "NOP", &a::NOP, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 7 },{ "???", &a::NOP, &a::IMP, 4 },{ "CMP", &a::CMP, &a::ABX, 4 },{ "DEC", &a::DEC, &a::ABX, 7 },{ "???", &a::XXX, &a::IMP, 7 },
    { "CPX", &a::CPX, &a::IMM, 2 },{ "SBC", &a::SBC, &a::IZX, 6 },{ "???", &a::NOP, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 8 },{ "CPX", &a::CPX, &a::ZP0, 3 },{ "SBC", &a::SBC, &a::ZP0, 3 },{ "INC", &a::INC, &a::ZP0, 5 },{ "???", &a::XXX, &a::IMP, 5 },{ "INX", &a::INX, &a::IMP, 2 },{ "SBC", &a::SBC, &a::IMM, 2 },{ "NOP", &a::NOP, &a::IMP, 2 },{ "???", &a::SBC, &a::IMP, 2 },{ "CPX", &a::CPX, &a::ABS, 4 },{ "SBC", &a::SBC, &a::ABS, 4 },{ "INC", &a::INC, &a::ABS, 6 },{ "???", &a::XXX, &a::IMP, 6 },
    { "BEQ", &a::BEQ, &a::REL, 2 },{ "SBC", &a::SBC, &a::IZY, 5 },{ "???", &a::XXX, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 8 },{ "???", &a::NOP, &a::IMP, 4 },{ "SBC", &a::SBC, &a::ZPX, 4 },{ "INC", &a::INC, &a::ZPX, 6 },{ "???", &a::XXX, &a::IMP, 6 },{ "SED", &a::SED, &a::IMP, 2 },{ "SBC", &a::SBC, &a::ABY, 4 },{ "NOP", &a::NOP, &a::IMP, 2 },{ "???", &a::XXX, &a::IMP, 7 },{ "???", &a::NOP, &a::IMP, 4 },{ "SBC", &a::SBC, &a::ABX, 4 },{ "INC", &a::INC, &a::ABX, 7 },{ "???", &a::XXX, &a::IMP, 7 },
    };
}

olc6502::~olc6502() {

}

uint8_t olc6502::read(uint16_t a) {
    return bus -> read(a, false);
}

void olc6502::write(uint16_t a, uint8_t d) {
    bus -> write(a, d)
}

void olc6502::clock() {
    /* Synchronous execution */
    if (cycles == 0) {
        /* Read the program counter, this returns one byte */
        instruction = read(pc);
        /* Increment program counter */
        pc = pc + 1;

        /* Get starting number of cycles for this instruction */
        cycles = lookup[instruction].cycles;
        /* Function call for required address mode */
        uint8_t additional_cycle1 = (this->*lookup[instruction].addrmode)();
        /* Function call for required operation */
        uint8_t additional_cycle2 = (this->*lookup[instruction].operate)();

        /* Some instructions need additional clock cycles and the functions
         * implementing those instructions return 1 if they do. If both of the
         * above variables indicate that, the instruction gets an additional
         * cycle. */
        cycles = (cycles) + (additional_cycle1 & additional_cycle2);
    }

    /* Everytime we call the clock() function, one cycle has elapsed so we
     * decrement the number of cycles elapsed. We are really executing an
     * instruction one point at a time while the real hardware will be executing
     * it over several clock cycles. */
    cycles--;
}

void olc6502::SetFlag(FLAGS6502 f, bool v) {
    if (v) {
        status |= f;
    } else {
        status &= ~f;
    }
}

/* Addressing modes */
/* The address variable is set so that the instruction knows where to read the
 * data from when it needs to. */

/* Implied means there's no data required to operate on. However it could
 * also mean the instruction is operating on the accumulator. */
uint8_t olc6502::IMP() {
    fetched = a;

    return 0;
}

/* In immediate mode, data is supplied as part of the instruction.  */
uint8_t olc6502::IMM() {
    addr_abs = pc;
    pc = pc + 1;

    return 0;
}

/* Pages are a way to organize memory. For a memory address, we require 16 bits.
 * The high byte is referred to as page while the low byte is called the offset.
 * The entire address space could be thought of as 256 pages of 256 bytes. */

/* In both zero-page and absolute addressing mode:
 * >> Address supplied as data -> Actual data
 * In indirect addressing mode:
 * >> Address supplied as data -> Address -> Actual data */

/* In zero-page addressing, the high byte is 0 -- the data could be found
 * somewhere in page 0. The address is supplied as data in the instruction
 * itself. (AND)ing with 0x00FF removes the high byte. Eg: 0xAABB (AND) 0x00FF
 * is 0x00BB. */
uint8_t olc6502::ZP0() {
    addr_abs = read(pc);
    pc = pc + 1;
    addr_abs &= 0x00FF;

    return 0;
}

/* In zero-page addressing with X offset, the contents of the X register are
 * added to the supplied address. */
uint8_t olc6502::ZPX() {
    addr_abs = (read(pc) + x);
    pc = pc + 1;
    addr_abs &= 0x00FF;

    return 0;
}

/* In zero-page addressing with Y offset, the contents of the Y register are
 * added to the supplied address. */
uint8_t olc6502::ZPY() {
    addr_abs = (read(pc) + y);
    pc = pc + 1;
    addr_abs &= 0x00FF;

    return 0;
}

/* In absolute addressing, the whole 16-bit address is used. The address is
 * supplied as data in the instruction itself. The high_byte (stored in pc + 1)
 * is shifted 8 units to the left (in binary, 2 units in hex) and (OR)ed with
 * the low_byte (stored in pc). This is because the 6502 is little-endian which
 * means that the data is first read in the lower byte.
 * Eg: 0x1234 will be read as (0x34) and then (0x12)
 * What the OR operation does is add the numbers together.
 * Eg: (0x00BB << 8) | (0x00CC)
 * => (0xBB00) | (0x00CC)
 * => 0xBBCC */
uint8_t olc6502::ABS() {
    uint16_t low_byte = read(pc);
    pc = pc + 1;
    uint16_t high_byte = read(pc);
    pc = pc + 1;

    addr_abs = (high_byte << 8) | low_byte;

    return 0;
}

/* Absolute addressing mode with X offset. The same as absolute addressing mode
 * but the contents of the X register are added to the computed address. This
 * introduces the case when the high byte changes (we move into a different
 * page) which then requires an additional clock cycle. Returning 1 is an
 * indication of that requirement. */
uint8_t olc6502::ABX() {
    uint16_t low_byte = read(pc);
    pc = pc + 1;
    uint16_t high_byte = read(pc);
    pc = pc + 1;

    addr_abs = (high_byte << 8) | low_byte;
    addr_abs = addr_abs + x;

    if ((addr_abs & 0xFF00) != (high_byte << 8)) {
        return 1;
    } else {
        return 0;
    }

    return 0;
}

/* Absolute addressing mode with Y offset. The same as absolute addressing mode
 * but the contents of the Y register are added to the computed address. This
 * introduces the case when the high byte changes (we move into a different
 * page) which then requires an additional clock cycle. Returning 1 is an
 * indication of that requirement. */
uint8_t olc6502::ABY() {
    uint16_t low_byte = read(pc);
    pc = pc + 1;
    uint16_t high_byte = read(pc);
    pc = pc + 1;

    addr_abs = (high_byte << 8) | low_byte;
    addr_abs = addr_abs + y;

    if ((addr_abs & 0xFF00) != (high_byte << 8)) {
        return 1;
    } else {
        return 0;
    }

    return 0;
}

/* In indirect addressing mode, the data itself is an address pointing to
 * another address storing the data. Think of it as a pointer of sorts. So we
 * have to effectively process the data twice. */
uint8_t olc6502::IND() {
    uint16_t ptr_low_byte = read(pc);
    pc = pc + 1;
    uint16_t ptr_high_byte = read(pc);
    pc = pc + 1;

    uint16_t ptr = (ptr_high_byte << 8) | ptr_low_byte;

    /* From https://nesdev.com/6502bugs.txt "An indirect JMP (xxFF) will fail
     * because the MSB will be fetched from address xx00 instead of page xx+1."
     * which is just a fancy way of saying that if we try fetching the higher
     * byte of 0xFF, we'll get 0x00 that is wrapped around from the same page
     * instead of getting it from the next page.
     * Following is the code to simulate this hardware bug */
    if (ptr_low_byte == 0x00FF) {
        uint16_t low_byte = read(ptr);
        ptr = ptr & 0xFF00;
        uint16_t high_byte = read(ptr);
        addr_abs = (high_byte << 8) | low_byte;
    } else {
        uint16_t low_byte = read(ptr);
        ptr = ptr + 1;
        uint16_t high_byte = read(ptr);
        addr_abs = (high_byte << 8) | low_byte;
    }

    return 0;
}
